const swaggerUi = require('swagger-ui-express');
const swaggerEnv = require('./src/environment/config/swagger');
// eslint-disable-next-line no-unused-vars
const mongo = require('./src/connections/mongodb.connection');

const express = require('express');
const app = express();
const port = process.env.PORT || 7008;

const shipmentRoutes = require('./src/routes/shipment.routes');
const messageSender = require('./src/eventhandlers/message.sender');
const messageReceiver = require('./src/eventhandlers/message.receiver');

const bodyParser = require('body-parser');

const rabbitmq = require('./src/connections/rabbitmq.connection');
const { eurekaClient } = require('./src/environment/config/eureka.config');

app.use(bodyParser.json());

// routes:
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerEnv));
app.use('/shipment', shipmentRoutes);

// default:
app.use('*', (req, res) => {
  res.status(404).send({
    error: 'not available',
  });
});

rabbitmq
  .connect()
  .then(channels => {
    messageSender.setMessageChannel(channels.sendChannel);
    messageReceiver.setMessageChannel(channels.receiveChannel);
  })
  .catch(err => {
    // eslint-disable-next-line no-console
    console.error(err);
  });

if (process.env.NODE_ENV === 'production') {
  // eslint-disable-next-line no-console
  console.log('Starting eureka connection...');
  eurekaClient.start();
}

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server running on port [${port}]`);
});

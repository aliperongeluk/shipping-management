const events = {
  SHIPMENT_COMPLETED: 'SHIPMENT_COMPLETED',
  ORDER_PICKED: 'ORDER_PICKED',
};

module.exports = events;

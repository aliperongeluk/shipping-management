const constants = {
  EXCHANGE_SHIPMENT: 'shipment',
  EXCHANGE_ORDER: 'order',
  SERVICE_NAME: 'shipment-management',
};

module.exports = constants;

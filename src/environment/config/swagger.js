const swaggerJsDoc = require('swagger-jsdoc');

const options = {
  swaggerDefinition: {
    info: {
      title: 'Shippping API Docs',
      version: '1.0',
      description:
        'API documentation for all endpoints regarding shipment management',
    },
  },

  apis: ['./src/routes/*.js', './src/models/*.js'],
};

const specs = swaggerJsDoc(options);

module.exports = specs;

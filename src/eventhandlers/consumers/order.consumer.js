const constants = require('../../environment/constants/constants');
const events = require('../../environment/constants/events');

const Shipment = require('../../models/shipment.model');

const consumeOrderPicked = messageChannel => {
  messageChannel
    .assertQueue(`${constants.SERVICE_NAME}_${events.ORDER_PICKED}`, {
      exclusive: false,
    })
    .then(queue => {
      messageChannel.bindQueue(
        queue.queue,
        constants.EXCHANGE_ORDER,
        events.ORDER_PICKED
      );

      messageChannel.consume(
        queue.queue,
        async message => {
          try {
            const messageObject = JSON.parse(message.content.toString());

            const shipment = new Shipment({
              orderId: messageObject._id,
              deliveryService: 'Mireurt',
            });

            await shipment.save();
          } catch (error) {
            // eslint-disable-next-line no-console
            console.log(error);
          }
        },
        {
          noAck: true,
        }
      );
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.log(error);
    });
};

const consume = messageChannel => {
  consumeOrderPicked(messageChannel);
};

module.exports = { consume };

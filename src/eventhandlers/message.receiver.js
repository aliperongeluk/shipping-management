const constants = require('../environment/constants/constants');
const orderConsumers = require('./consumers/order.consumer');

let messageChannel;

const startConsuming = () => {
  createExchanges();

  orderConsumers.consume(messageChannel);
};

const createExchanges = () => {
  messageChannel.assertExchange(constants.EXCHANGE_ORDER, 'topic', {
    durable: true,
  });
};

const setMessageChannel = channel => {
  messageChannel = channel;
  startConsuming();
};

module.exports = { setMessageChannel };

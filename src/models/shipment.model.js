const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * @swagger
 * definitions:
 *  Shipment:
 *    type: object
 *    properties:
 *      ShipmentStatus:
 *        type: string
 *      orderId:
 *        type: string
 *      deliveryService:
 *        type: string
 */

const ShipmentSchema = new Schema(
  {
    shipmentStatus: {
      type: String,
      default: 'NOT_SHIPPED',
      enum: ['NOT_SHIPPED', 'SHIPPED', 'DELIVERED'],
      required: [true, 'A status is required.'],
    },
    orderId: {
      type: Schema.Types.ObjectId,
      required: [true, 'An order id is required.'],
    },
    deliveryService: {
      type: String,
      required: [true, 'A delivery service is required.'],
    },
  },
  {
    timestamps: true,
  }
);

const Shipment = mongoose.model('shipment', ShipmentSchema);

module.exports = Shipment;

const express = require('express');
const routes = express.Router();

const Shipment = require('../models/shipment.model');
const events = require('../environment/constants/events');
const messageSender = require('../eventhandlers/message.sender');

/**
 * @swagger
 * /api/shipping-management/shipment/{id}/:
 *    get:
 *     tags:
 *       - shipment (microservice mongo)
 *     description: Returns shipment with given ID from database
 *     produces:
 *       - application/json
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: object
 *          items:
 *            $ref: '#/definitions/Shipment'
 *        required: true
 *        description: String ID of the shipment.
 *     responses:
 *       200:
 *          description: Successfully loaded shipment.
 *       400:
 *          description: Failed to load shipment.
 */
routes.get('/:id', (req, res) => {
  const id = req.params.id;

  Shipment.findById(id)
    .then(shipment => {
      if (shipment != null) {
        res.status(200).send(shipment);
      } else {
        res.status(404).send({
          message: 'Shipment not found.',
        });
      }
    })
    .catch(error => {
      res.status(400).send(error);
    });
});

/**
 * @swagger
 * /api/shipment-management/shipment/:
 *    get:
 *     tags:
 *       - shipment (microservice mongo)
 *     description: Returns all shipments
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Successfully loaded shipments.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Shipment'
 *       400:
 *          description: Failed to load shipments.
 */
routes.get('/', (req, res) => {
  Shipment.find({})
    .then(shipments => {
      res.status(200).send(shipments);
    })
    .catch(error => {
      res.status(400).send(error);
    });
});

/**
 * @swagger
 * /api/shipment-management/shipment/:
 *    post:
 *      tags:
 *        - shipment (microservice mongo)
 *      description: Creates a shipment (soon only possible through authentication microservice)
 *      produces:
 *        - application/json
 *      parameters:
 *        - in: body
 *          name: body
 *          description: Set initial shipment attributes
 *          schema:
 *            type: object
 *            properties:
 *              orderId:
 *                type: string
 *              deliveryService:
 *                type: string
 *      responses:
 *        200:
 *          description: Shipment added successfully.
 *        400:
 *          description: Failed to create shipment.
 */
routes.post('/', (req, res) => {
  const newshipment = new Shipment(req.body);

  Shipment.findOne({ orderId: req.body.orderId })
    .then(shipment => {
      if (shipment == null) {
        newshipment
          .save()
          .then(createdShipment => {
            res.status(200).send(createdShipment);
          })
          .catch(error => {
            res.status(500).send(error);
          });
      } else {
        res.status(400).send({
          message:
            'Something went wrong. Possibly, a shipment with the given orderId already exists.',
        });
      }
    })
    .catch(error => {
      res.status(500).send(error);
    });
});

/**
 * @swagger
 * /api/shipment-management/shipment/{id}/:
 *    put:
 *      tags:
 *        - shipment (microservice mongo)
 *      description: Creates a shipment (soon only possible through authentication microservice)
 *      produces:
 *        - application/json
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: String ID of the shipment.
 *        - in: body
 *          name: body
 *          description: Update shipment attributes
 *          schema:
 *            type: object
 *            properties:
 *              shipmentStatus:
 *                type: string
 *              orderId:
 *                type: string
 *              deliveryService:
 *                type: string
 *      responses:
 *        200:
 *          description: Changes were successfully made.
 *        400:
 *          description: Failed to make changes to shipment.
 */
routes.put('/:id', (req, res) => {
  const id = req.params.id;

  Shipment.findById(id)
    .then(shipment => {
      shipment
        .updateOne(req.body)
        .then(() => {
          Shipment.findById(id)
            .then(shipment => {
              if (shipment.shipmentStatus === 'SHIPPED') {
                messageSender.publish(events.SHIPMENT_COMPLETED, shipment);
              }
              res.status(200).send(shipment);
            })
            .catch(error => {
              res.status(400).send({
                error: error,
                message: 'asda',
              });
            });
        })
        .catch(error => {
          res.status(400).send(error);
        });
    })
    .catch(error => {
      res.status(400).send(error);
    });
});

module.exports = routes;
